package hr.ttkalcevic.githubsearch.ui.splash

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import hr.ttkalcevic.githubsearch.ui.base.BasePresenter

@Suppress("unused")
class SplashPresenter : BasePresenter<SplashViewInterface>(), SplashPresenterInterface {

    override fun checkInternetConnection(context: Context) {
        val connected = isNetworkAvailable(context)
        if (connected) {
            getView()?.goToSearch()
        } else {
            getView()?.noInternetError()
        }
    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val activeNetworkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

}
