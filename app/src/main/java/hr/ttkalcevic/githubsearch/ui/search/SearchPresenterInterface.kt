package hr.ttkalcevic.githubsearch.ui.search

import hr.ttkalcevic.githubsearch.data.model.Repository
import hr.ttkalcevic.githubsearch.data.model.User
import hr.ttkalcevic.githubsearch.ui.base.BasePresenterInterface


@Suppress("unused")
interface SearchPresenterInterface : BasePresenterInterface<SearchViewInterface> {

    fun loadRepositories(page: Int = 0, query: String = "", sortId: Int = 0, clear: Boolean = false)

    fun onProfileClick(user: User)

    fun onRepositoryClick(repository: Repository)

}
