package hr.ttkalcevic.githubsearch.ui.search.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.ttkalcevic.githubsearch.R
import hr.ttkalcevic.githubsearch.data.model.Repository
import hr.ttkalcevic.githubsearch.data.model.User
import hr.ttkalcevic.githubsearch.databinding.ItemRepositoryBinding

@Suppress("unused")
class RepositoryRecyclerAdapter() : RecyclerView.Adapter<RepositoryViewHolder>() {

    var items: ArrayList<Repository> = ArrayList()

    interface ClickListener {
        fun onRepositoryClick(repository: Repository)

        fun onProfileClick(user: User)
    }

    var clickListener: ClickListener = object : ClickListener {
        override fun onRepositoryClick(repository: Repository) {
        }

        override fun onProfileClick(user: User) {
        }
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    fun add(repository: Repository) {
        items.add(repository)
        notifyItemInserted(itemCount - 1)
    }

    fun add(list: List<Repository>) {
        items.addAll(list)
        notifyItemRangeInserted(itemCount - list.size, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRepositoryBinding.inflate(inflater)
        return RepositoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        holder.bind(items[position], clickListener)
    }


}
