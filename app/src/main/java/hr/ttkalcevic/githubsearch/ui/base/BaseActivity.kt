package hr.ttkalcevic.githubsearch.ui.base

import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import hr.ttkalcevic.githubsearch.R
import hr.ttkalcevic.githubsearch.ui.search.SearchActivity

abstract class BaseActivity : AppCompatActivity(), NetworkViewInterface {

    companion object {
        const val TAG: String = "BaseActivity"
    }

    fun startSearchActivity() {
        val searchIntent = Intent()
        searchIntent.setClass(this, SearchActivity::class.java)
        searchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(searchIntent)
    }

    override fun showNoInternetConnectionError() {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.error_title))
                .setMessage(getString(R.string.error_no_internet_message))
                .setPositiveButton(getString(R.string.common_close)) { dialog, _ ->
                    dialog.dismiss()
                    this.finish()
                }
                .show()
        Log.e(TAG, "no Internet connection")
    }

    override fun networkError() {
        Toast.makeText(this, getString(R.string.error_network_message), Toast.LENGTH_SHORT)
                .show()
        Log.e(TAG, "network error")
    }

    override fun unknownError() {
        Toast.makeText(this, getString(R.string.error_unknown_message), Toast.LENGTH_SHORT)
                .show()
        Log.e(TAG, "unknown error")
    }
}
