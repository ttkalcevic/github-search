package hr.ttkalcevic.githubsearch.ui.splash

import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import hr.ttkalcevic.githubsearch.R
import hr.ttkalcevic.githubsearch.ui.base.BaseActivity

class SplashActivity : BaseActivity(), SplashViewInterface {

    private lateinit var presenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        presenter = SplashPresenter()
        presenter.attachView(this)

        Handler().postDelayed({
            presenter.checkInternetConnection(this)
        }, 2500)
    }

    override fun showLoadingProgress() {
    }

    override fun hideLoadingProgress() {
    }

    override fun goToSearch() {
        startSearchActivity()
    }

    override fun noInternetError() {
        showNoInternetConnectionError()
    }
}
