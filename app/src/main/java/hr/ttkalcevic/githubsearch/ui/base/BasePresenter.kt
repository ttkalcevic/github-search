package hr.ttkalcevic.githubsearch.ui.base

@Suppress("unused")
open class BasePresenter<V : NetworkViewInterface> : BasePresenterInterface<V> {

    var attachedView: V? = null

    override fun attachView(view: V) {
        attachedView = view
    }

    override fun getView(): V? {
        return attachedView
    }

    override fun detachView() {
        attachedView = null
    }
}
