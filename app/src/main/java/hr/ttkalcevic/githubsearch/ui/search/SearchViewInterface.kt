package hr.ttkalcevic.githubsearch.ui.search

import hr.ttkalcevic.githubsearch.data.model.Repository
import hr.ttkalcevic.githubsearch.ui.base.NetworkViewInterface

@Suppress("unused")
interface SearchViewInterface : NetworkViewInterface {

    fun clearList()

    fun addRepositories(repoList: List<Repository>)

    fun goToProfileDetails()

    fun goToRepositoryDetails()
}
