package hr.ttkalcevic.githubsearch.ui.splash

import android.content.Context
import hr.ttkalcevic.githubsearch.ui.base.BasePresenterInterface

@Suppress("unused")
interface SplashPresenterInterface : BasePresenterInterface<SplashViewInterface> {

    fun checkInternetConnection(context: Context)

}
