package hr.ttkalcevic.githubsearch.ui.search

import hr.ttkalcevic.githubsearch.api.callback.ApiCallbackWrapper
import hr.ttkalcevic.githubsearch.data.model.Repository
import hr.ttkalcevic.githubsearch.data.model.Sort
import hr.ttkalcevic.githubsearch.data.model.User
import hr.ttkalcevic.githubsearch.data.source.RemoteDataSource
import hr.ttkalcevic.githubsearch.ui.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Suppress("unused")
@Singleton
class SearchPresenter() : BasePresenter<SearchViewInterface>(), SearchPresenterInterface {

    private lateinit var dataSource: RemoteDataSource

    @Inject
    constructor(dataSource: RemoteDataSource) : this() {
        this.dataSource = dataSource
    }

    override fun loadRepositories(page: Int, query: String, sortId: Int, clear: Boolean) {
        var query: String = query
        if (query.isBlank())
            query = "a"

        getView()?.showLoadingProgress()
        dataSource.searchRepositories(page, query, Sort.findById(sortId)!!.value)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiCallbackWrapper<List<Repository>>() {
                    override fun onNext(t: List<Repository>) {
                        if (clear)
                            getView()?.clearList()
                        getView()?.addRepositories(t)
                        getView()?.hideLoadingProgress()
                    }

                    override fun onNetworkError() {
                        getView()?.networkError()
                        getView()?.hideLoadingProgress()
                    }

                    override fun onUnknownError() {
                        getView()?.unknownError()
                        getView()?.hideLoadingProgress()
                    }
                })
    }

    override fun onProfileClick(user: User) {
        getView()?.goToProfileDetails()
    }

    override fun onRepositoryClick(repository: Repository) {
        getView()?.goToRepositoryDetails()
    }
}
