package hr.ttkalcevic.githubsearch.ui.base

@Suppress("unused")
interface NetworkViewInterface {

    fun showNoInternetConnectionError()

    fun showLoadingProgress()
    fun hideLoadingProgress()

    fun networkError()
    fun unknownError()
}
