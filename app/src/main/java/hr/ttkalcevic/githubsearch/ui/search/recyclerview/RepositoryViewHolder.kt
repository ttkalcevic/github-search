package hr.ttkalcevic.githubsearch.ui.search.recyclerview

import androidx.recyclerview.widget.RecyclerView
import hr.ttkalcevic.githubsearch.data.model.Repository
import hr.ttkalcevic.githubsearch.databinding.ItemRepositoryBinding

class RepositoryViewHolder(val binding: ItemRepositoryBinding)
    : RecyclerView.ViewHolder(binding.root) {


    fun bind(repository: Repository, clickListener: RepositoryRecyclerAdapter.ClickListener) {
        binding.repository = repository
        binding.executePendingBindings()
    }
}
