package hr.ttkalcevic.githubsearch.ui.search

import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.paginate.Paginate
import dagger.android.AndroidInjection
import hr.ttkalcevic.githubsearch.R
import hr.ttkalcevic.githubsearch.data.model.Repository
import hr.ttkalcevic.githubsearch.data.model.User
import hr.ttkalcevic.githubsearch.ui.base.BaseActivity
import hr.ttkalcevic.githubsearch.ui.search.recyclerview.RepositoryRecyclerAdapter
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject

class SearchActivity : BaseActivity(), SearchViewInterface, RepositoryRecyclerAdapter.ClickListener {

    @Inject
    lateinit var presenter: SearchPresenter
    @Inject
    lateinit var adapter: RepositoryRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)

        setContentView(R.layout.activity_search)
        presenter.attachView(this)
        adapter.clickListener = this

        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val itemDecorator = DividerItemDecoration(this, layoutManager.orientation)

        recyclerRepos.adapter = adapter
        recyclerRepos.layoutManager = layoutManager
        recyclerRepos.addItemDecoration(itemDecorator)


        presenter.loadRepositories()
    }


    override fun showLoadingProgress() {
        refreshRepos.isRefreshing = true
    }

    override fun hideLoadingProgress() {
        refreshRepos.isRefreshing = false
    }

    override fun clearList() {
        adapter.clear()
    }

    override fun addRepositories(repoList: List<Repository>) {
        adapter.add(repoList)
    }

    override fun goToProfileDetails() {
    }

    override fun goToRepositoryDetails() {
    }

    override fun onRepositoryClick(repository: Repository) {
    }

    override fun onProfileClick(user: User) {
    }
}
