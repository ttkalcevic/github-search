package hr.ttkalcevic.githubsearch.ui.splash

import hr.ttkalcevic.githubsearch.ui.base.NetworkViewInterface


@Suppress("unused")
interface SplashViewInterface : NetworkViewInterface {

    fun goToSearch()
    fun noInternetError()

}
