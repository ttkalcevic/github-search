package hr.ttkalcevic.githubsearch.ui.base

@Suppress("unused")
interface BasePresenterInterface<V : NetworkViewInterface> {

    fun attachView(view: V)
    fun getView(): V?
    fun detachView()

}
