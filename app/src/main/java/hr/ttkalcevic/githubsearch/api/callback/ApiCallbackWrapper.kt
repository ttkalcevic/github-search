package hr.ttkalcevic.githubsearch.api.callback

import io.reactivex.disposables.Disposable

@Suppress("unused")
abstract class ApiCallbackWrapper<T> : ApiCallback<T>() {

    override fun onSubscribe(d: Disposable) {
    }

    override fun onNext(t: T) {
    }

    override fun onComplete() {
    }

    override fun onNetworkError() {
    }

    override fun onUnknownError() {
    }
}
