package hr.ttkalcevic.githubsearch.api.callback

import io.reactivex.Observer
import java.io.IOException

@Suppress("unused")
abstract class ApiCallback<T> : Observer<T> {

    override fun onError(exception: Throwable) {
        when (exception) {
            is IOException -> {
                onNetworkError()
            }
            else -> {
                onUnknownError()
            }
        }
    }

    abstract fun onNetworkError()

    abstract fun onUnknownError()
}
