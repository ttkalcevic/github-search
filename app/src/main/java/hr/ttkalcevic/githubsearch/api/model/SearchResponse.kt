package hr.ttkalcevic.githubsearch.api.model

import com.google.gson.annotations.SerializedName
import hr.ttkalcevic.githubsearch.data.model.Repository

@Suppress("unused")
data class SearchResponse(
        @SerializedName("total_count") val count: Int,
        @SerializedName("items") val items: List<Repository>)
