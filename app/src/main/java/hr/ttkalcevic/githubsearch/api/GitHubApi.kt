package hr.ttkalcevic.githubsearch.api

import hr.ttkalcevic.githubsearch.api.model.SearchResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

@Suppress("unused")
interface GitHubApi {

    companion object {
        const val API_URL = "https://api.github.com/"
    }

    @GET("search/repositories")
    fun searchRepositories(
            @Query("page") page: Int,
            @Query("q") query: String,
            @Query("sort") sort: String,
            @Query("per_page") perPage: Int = 30): Observable<SearchResponse>
}
