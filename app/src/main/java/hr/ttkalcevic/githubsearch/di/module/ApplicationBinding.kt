package hr.ttkalcevic.githubsearch.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import hr.ttkalcevic.githubsearch.di.scope.ActivityScope
import hr.ttkalcevic.githubsearch.ui.search.SearchActivity
import hr.ttkalcevic.githubsearch.ui.splash.SplashActivity

@Suppress("unused")
@Module
abstract class ApplicationBinding {

    @ContributesAndroidInjector(modules = [(SplashModule::class)])
    @ActivityScope
    abstract fun splashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [(SearchModule::class)])
    @ActivityScope
    abstract fun searchActivity(): SearchActivity
}
