package hr.ttkalcevic.githubsearch.di.module

import dagger.Module
import dagger.Provides
import hr.ttkalcevic.githubsearch.di.scope.ActivityScope
import hr.ttkalcevic.githubsearch.ui.search.SearchPresenter
import hr.ttkalcevic.githubsearch.ui.search.SearchPresenterInterface
import hr.ttkalcevic.githubsearch.ui.search.recyclerview.RepositoryRecyclerAdapter

@Suppress("unused")
@Module
class SearchModule {

    @Provides
    @ActivityScope
    fun provideSearchPresenter(): SearchPresenterInterface {
        return SearchPresenter()
    }

    @Provides
    @ActivityScope
    fun provideReporitoryAdapter(): RepositoryRecyclerAdapter {
        return RepositoryRecyclerAdapter()
    }
}
