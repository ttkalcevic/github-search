package hr.ttkalcevic.githubsearch.di.module

import dagger.Module
import dagger.Provides
import hr.ttkalcevic.githubsearch.di.scope.ActivityScope
import hr.ttkalcevic.githubsearch.ui.splash.SplashPresenter
import hr.ttkalcevic.githubsearch.ui.splash.SplashPresenterInterface

@Suppress("unused")
@Module
class SplashModule {

    @Provides
    @ActivityScope
    fun provideSplashPresenter(): SplashPresenterInterface {
        return SplashPresenter()
    }
}
