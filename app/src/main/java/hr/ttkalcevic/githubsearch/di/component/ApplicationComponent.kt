package hr.ttkalcevic.githubsearch.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import hr.ttkalcevic.githubsearch.GitHubSearch
import hr.ttkalcevic.githubsearch.di.module.ApplicationBinding
import hr.ttkalcevic.githubsearch.di.module.ApplicationModule
import javax.inject.Singleton

@Suppress("unused")
@Component(modules = [
    (AndroidInjectionModule::class),
    (ApplicationBinding::class),
    (ApplicationModule::class)
])

@Singleton
interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: GitHubSearch)
}
