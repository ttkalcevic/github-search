package hr.ttkalcevic.githubsearch.di.scope

import javax.inject.Scope

@Suppress("unused")
@Scope
annotation class ActivityScope
