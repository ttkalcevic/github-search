package hr.ttkalcevic.githubsearch.di.module


import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import hr.ttkalcevic.githubsearch.api.GitHubApi
import hr.ttkalcevic.githubsearch.data.source.RemoteDataSource
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Suppress("unused")
@Module
class ApplicationModule() {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun provideGitHubRetrofit(): Retrofit {
        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(GitHubApi.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideGitHubService(retrofit: Retrofit): GitHubApi {
        return retrofit.create(GitHubApi::class.java)
    }

    @Provides @Singleton
    fun provideRemoteDataSource(gitHubApi: GitHubApi):RemoteDataSource {
        return RemoteDataSource(gitHubApi)
    }

}
