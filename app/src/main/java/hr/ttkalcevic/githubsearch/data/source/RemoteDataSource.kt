package hr.ttkalcevic.githubsearch.data.source

import hr.ttkalcevic.githubsearch.api.GitHubApi
import hr.ttkalcevic.githubsearch.data.model.Repository
import io.reactivex.Observable
import javax.inject.Inject

@Suppress("unused")
class RemoteDataSource @Inject constructor(var gitHubApi: GitHubApi) : DataSourceInterface {

    override fun searchRepositories(page: Int, query: String, sort: String)
            : Observable<List<Repository>> {
        return gitHubApi.searchRepositories(page, query, sort)
                .map { it -> it.items }
    }

}
