package hr.ttkalcevic.githubsearch.data.model

@Suppress("unused")
enum class Sort(val id: Int, val value: String) {

    STARS(0, "stars"),
    FORKS(1, "forks"),
    UPDATED(2, "updated");

    companion object {
        fun findById(id: Int ): Sort? {
            for (sort in values()) {
                if( sort.id == id) {
                    return sort
                }
            }

            return null
        }
    }
}
