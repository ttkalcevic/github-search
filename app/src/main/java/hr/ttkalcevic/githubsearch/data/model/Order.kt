package hr.ttkalcevic.githubsearch.data.model

@Suppress("unused")
enum class Order(val id: Int, val value: String) {

    ASCENDING(0, "asc"),
    DESCENDING(1, "desc");

    companion object {
        fun findById(id: Int): Order? {
            for (order in values()) {
                if (order.id == id) {
                    return order
                }
            }

            return DESCENDING
        }
    }
}
