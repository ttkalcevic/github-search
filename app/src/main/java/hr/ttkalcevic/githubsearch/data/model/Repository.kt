package hr.ttkalcevic.githubsearch.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Suppress("unused")
@Parcelize
data class Repository(
        @SerializedName("id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("description") val description: String,
        @SerializedName("html_url") val url: String,
        @SerializedName("watchers_count") val watchers: Int,
        @SerializedName("forks_count") val forks: Int,
        @SerializedName("open_issues_count") val issues: Int,
        @SerializedName("language") val language: String,
        @SerializedName("owner") val user: User,
        @SerializedName("created_at") val createdOn: Date,
        @SerializedName("updated_at") val updatedOn: Date
) : Parcelable
