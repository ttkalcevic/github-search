package hr.ttkalcevic.githubsearch.data.source

import hr.ttkalcevic.githubsearch.data.model.Repository
import io.reactivex.Observable

@Suppress("unused")
interface DataSourceInterface {

    fun searchRepositories(page: Int, query: String, sort: String)
            : Observable<List<Repository>>
}
